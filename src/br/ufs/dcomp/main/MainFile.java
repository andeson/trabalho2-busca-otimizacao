
package br.ufs.dcomp.main;

import br.ufs.dcomp.algorithim.Tweak;
import br.ufs.dcomp.function.FAckleyHillClimbing;
import br.ufs.dcomp.function.FAckleyILS;
import br.ufs.dcomp.function.RosenbrockHillClimbing;
import br.ufs.dcomp.function.RosenbrockILS;
import br.ufs.dcomp.function.SphereHillClimbing;
import br.ufs.dcomp.function.SphereILS;
import br.ufs.dcomp.function.RotadedSchaffersHillClimbing;
import br.ufs.dcomp.function.RotadedSchaffersILS;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

public class MainFile {
    
    
    public static void main(String[] args) throws IOException {      
        
        exe_10_hillClimbing();
        exe_10_ils();
    }
    
    
    public static void exe_10_hillClimbing() throws IOException{
        int interaction = 10_000; 
        File file = new File("10_exe_HillClimbing.txt");
        file.createNewFile();
        // Prepara para escrever no arquivo
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        Locale localeBR = new Locale( "pt", "BR" );
        NumberFormat numeroBR = NumberFormat.getNumberInstance(localeBR);
        numeroBR.setMinimumFractionDigits(10);
        numeroBR.setMaximumFractionDigits(10);
        bw.write("Sphere;FAckley;Rosenbrock;RotadedSchaffers \r\n");
        
        
        for (int i = 0; i < 10; i++) {            
                Tweak tweak1 = new Tweak(0.01,  1);
                SphereHillClimbing alg1 = new SphereHillClimbing(30, -100, 100, tweak1);
                double valg1[] = alg1.exe(interaction);
                bw.write(numeroBR.format(alg1.quality(valg1)) + ";");
                
                Tweak tweak2 = new Tweak(0.1,  2);
                FAckleyHillClimbing alg5 = new FAckleyHillClimbing(30, -100, 100, tweak2);
                double valg5[] = alg5.exe(interaction);
                bw.write(numeroBR.format(alg5.quality(valg5)) + ";");
                
                Tweak tweak3 = new Tweak(0.01,  1);
                RosenbrockHillClimbing alg9 = new RosenbrockHillClimbing(30, -100, 100, tweak3);
                double valg9[] = alg9.exe(interaction);
                bw.write(numeroBR.format(alg9.quality(valg9)) + ";");
                
                Tweak tweak4 = new Tweak(0.01,  1);
                RotadedSchaffersHillClimbing alg13 = new RotadedSchaffersHillClimbing(30, -100, 100, tweak4);
                double valg13[] = alg13.exe(interaction);
                bw.write(numeroBR.format(alg13.quality(valg13)) + " \r\n");
        }
        
        bw.close();
        fw.close();
        
        
        
    }
        
    public static void exe_10_ils() throws IOException{
        int interaction = 10_000; 
        File file = new File("10_exe_ils.txt");
        file.createNewFile();
        // Prepara para escrever no arquivo
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        Locale localeBR = new Locale( "pt", "BR" );
        NumberFormat numeroBR = NumberFormat.getNumberInstance(localeBR);
        numeroBR.setMinimumFractionDigits(10);
        numeroBR.setMaximumFractionDigits(10);
        bw.write("Sphere;FAckley;Rosenbrock;RotadedSchaffers \r\n");
        
        
        for (int i = 0; i < 10; i++) {
            Tweak tweak1 = new Tweak(0.01,  1);
            SphereILS alg1 = new SphereILS(30, -100, 100, tweak1, 0.01, 1, 5);
            double valg1[] = alg1.exe(interaction / 5);
            bw.write(numeroBR.format(alg1.quality(valg1)) + ";");
            
            Tweak tweak2 = new Tweak(0.01,  1);
            FAckleyILS alg2 = new FAckleyILS(30, -100, 100, tweak2, 0.01, 4, 20);
            double valg2[] = alg2.exe(interaction / 20);
            bw.write(numeroBR.format(alg2.quality(valg2)) + ";");            
            

            Tweak tweak3 = new Tweak(0.01,  1);
            RosenbrockILS alg9 = new RosenbrockILS(30, -100, 100, tweak3, 0.01, 4, 20);
            double valg9[] = alg9.exe(interaction / 20);
            bw.write(numeroBR.format(alg9.quality(valg9)) + ";");

            Tweak tweak4 = new Tweak(0.01,  1);
            RotadedSchaffersILS alg4 = new RotadedSchaffersILS(30, -100, 100, tweak3, 0.01, 4, 20);
            double valg4[] = alg4.exe(interaction / 20);
            bw.write(numeroBR.format(alg4.quality(valg4)) + "\r\n");
        }
        
        bw.close();
        
        
    }
    
    public static void ILSWriteFile() throws IOException{
        double [] p1Values = {0.01, 0.1, 0.5, 1.0}; //Parametros p - probavilidade
        double [] r1Values = {1, 2, 4, 6};  //Parametro r - Range
        double [] p2Values = {0.01, 0.1, 0.5, 1.0}; //Parametros p - probavilidade do perturbe
        double [] r2Values = {1, 2, 4, 6};  //Parametro r - Range do perturbe
        int [] time = {5,20,100,1000};  //Parametro r - Range do perturbe
        
        int interaction = 10000; 
        File file = new File("ils.txt");
        file.createNewFile();
        // Prepara para escrever no arquivo
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        Locale localeBR = new Locale( "pt", "BR" );
        NumberFormat numeroBR = NumberFormat.getNumberInstance(localeBR);
        numeroBR.setMinimumFractionDigits(10);
        numeroBR.setMaximumFractionDigits(10);
        bw.write("Combinacao;Sphere;FAckley;Rosenbrock;RotadedSchaffers\r\n");
        
         for (int i = 0; i < p1Values.length; i++) {
             for (int j = 0; j < r1Values.length; j++) {
                 for (int k = 0; k < p2Values.length; k++) {
                     for (int l = 0; l < r2Values.length; l++) {
                         for (int m = 0; m < time.length; m++) {
                             bw.write("p1= " + p1Values[i] + ", r1= " + r1Values[j] + ", p2= " + p2Values[k] + ", r2= " + r2Values[l] + ", t= " + time[m] + ";");

                             Tweak tweak = new Tweak(p1Values[i], r1Values[j]);

                             SphereILS alg1 = new SphereILS(30, -100, 100, tweak, p2Values[k], r2Values[l], time[m]);
                             double valg1[] = alg1.exe(interaction / time[m]);
                             bw.write(numeroBR.format(alg1.quality(valg1)) + ";");


                             FAckleyILS alg2 = new FAckleyILS(30, -100, 100, tweak, p2Values[k], r2Values[l], time[m]);
                             double valg2[] = alg2.exe(interaction / time[m]);
                             bw.write(numeroBR.format(alg2.quality(valg2)) + ";");
                             
                             RosenbrockILS alg9 = new RosenbrockILS(30, -100, 100, tweak, p2Values[k], r2Values[l], time[m]);
                             double valg9[] = alg9.exe(interaction / time[m]);
                             bw.write(numeroBR.format(alg9.quality(valg9)) + ";");

                             
                             RotadedSchaffersILS alg4 = new RotadedSchaffersILS(30, -100, 100, tweak, p2Values[k], r2Values[l], time[m]);
                             double valg4[] = alg4.exe(interaction / time[m]);
                             bw.write(numeroBR.format(alg4.quality(valg4)) + "; \r\n");


                         }

                     }

                 }
             }
         }
         bw.close();
    }
    
    public static void hillClimbingWriteFile() throws IOException{
        double [] pValues = {0.01, 0.1, 0.5, 1.0}; //Parametros p - probavilidade
        double [] rValues = {1, 2, 4, 6};  //Parametro r - Range
        int interaction = 10000; 
        File file = new File("hillClimbing.txt");
        file.createNewFile();
        // Prepara para escrever no arquivo
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        Locale localeBR = new Locale( "pt", "BR" );
        NumberFormat numeroBR = NumberFormat.getNumberInstance(localeBR);
        numeroBR.setMinimumFractionDigits(10);
        numeroBR.setMaximumFractionDigits(10);
        bw.write("Combinacao;Sphere;FAckley;Rosenbrock;RotadedSchaffers\r\n");
        
        for (int i = 0; i < pValues.length; i++) {
            for (int j = 0; j < rValues.length; j++) {                
                bw.write("p= " + numeroBR.format(pValues[i]) +" e r= "+numeroBR.format(rValues[j]) + ";");
                
                Tweak tweak = new Tweak(pValues[i],  rValues[j]);    
                
                SphereHillClimbing alg1 = new SphereHillClimbing(30, -100, 100, tweak);
                double valg1[] = alg1.exe(interaction);
                bw.write(numeroBR.format(alg1.quality(valg1)) + ";");
                
                FAckleyHillClimbing alg2 = new FAckleyHillClimbing(30, -100, 100, tweak);
                double valg2[] = alg2.exe(interaction);
                bw.write(numeroBR.format(alg2.quality(valg2)) + ";");                
                
                RosenbrockHillClimbing alg9 = new RosenbrockHillClimbing(30, -100, 100, tweak);
                double valg9[] = alg9.exe(interaction);
                bw.write(numeroBR.format(alg9.quality(valg9)) + ";");
                
                RotadedSchaffersHillClimbing alg4 = new RotadedSchaffersHillClimbing(30, -100, 100, tweak);
                double valg4[] = alg4.exe(interaction);
                double num = alg4.quality(valg4);
                bw.write(numeroBR.format(num) + "; \r\n");
                

                
            }            
        }
         bw.close();
         fw.close();
 
    }

}
