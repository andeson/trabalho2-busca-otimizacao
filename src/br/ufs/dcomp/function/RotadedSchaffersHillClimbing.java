/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufs.dcomp.function;

import br.ufs.dcomp.algorithim.HillClimbing;
import br.ufs.dcomp.algorithim.Tweak;

public class RotadedSchaffersHillClimbing extends HillClimbing {

    public RotadedSchaffersHillClimbing(int lengthVector, int minValueArray, int maxValueArray, Tweak tweak) {
        super(lengthVector, minValueArray, maxValueArray, tweak);
    }

    @Override
    public double quality(double[] input) {
        double sum = 0;

        for (int i = 0; i < input.length - 1; i++) {
            double x = input[i];
            double x1 = input[i + 1];
            double si = Math.sqrt(x * x + x1 * x1);

            double sinTerm = Math.sin(50 * Math.pow(si, 0.2));
            sum += Math.sqrt(si) + Math.sqrt(si) * sinTerm * sinTerm;
        }

        sum /= input.length - 1;
        return sum * sum;
    }

}
