/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufs.dcomp.function;

import br.ufs.dcomp.algorithim.ILS;
import br.ufs.dcomp.algorithim.Tweak;


public class FAckleyILS extends ILS{

    public FAckleyILS(int lengthVector, int minValueArray, int maxValueArray, Tweak tweak, double pertube, double range, int time) {
        super(lengthVector, minValueArray, maxValueArray, tweak, pertube, range, time);
    }

    @Override
    public double quality(double[] x) {
        double sum1 = 0.0, sum2 = 0.0, result;
        for (int i = 0; i < x.length; ++i) {
            sum1 += x[i] * x[i];
            sum2 += Math.cos(2.0 * Math.PI * x[i]);
        }
        sum1 = -0.2 * Math.sqrt(sum1 / x.length);
        sum2 /= x.length;
        result = 20.0 + Math.E - 20.0 * Math.exp(sum1) - Math.exp(sum2);
        return result;
    }
    
}
