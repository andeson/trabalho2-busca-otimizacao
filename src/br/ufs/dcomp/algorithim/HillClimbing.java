/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufs.dcomp.algorithim;


public abstract class HillClimbing extends Generic{

   
    public HillClimbing(int lengthVector, int minValueArray, int maxValueArray, Tweak tweak) {
        super(lengthVector, minValueArray, maxValueArray, tweak);
    }  
    
   
    @Override
    public double[] exe(int interaction){
        int cont = 0;
        double[] S = initSolution();
        double[] R ; 
       
        do {  
            R = tweak(S.clone());
            if (quality(R) < quality(S)) {
                S = R;                    
            }
            cont ++;
            printQuality(quality(S), cont);
        } while (cont < interaction || quality(S) == 0);
        
        return S;
    }



}